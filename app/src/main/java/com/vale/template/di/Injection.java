package com.vale.template.di;

import android.content.Context;

import com.vale.template.BuildConfig;
import com.vale.template.DefaultApplication;
import com.vale.template.data.DataRepository;
import com.vale.template.data.local.LocalDataSource;
import com.vale.template.data.remote.ApiService;
import com.vale.template.data.remote.RemoteDataSource;
import com.vale.template.utils.NetworkHelper;
import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by owson on 5/29/18.
 */

public class Injection {
    public static DataRepository provideDataRepository(MainUiThread mainUiThread,
                                                       ThreadExecutor threadExecutor) {


        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                HttpUrl httpUrl = chain.request().url().newBuilder()
                        .build();
                Request.Builder builder =  chain.request().newBuilder().url(httpUrl);

                /*Context context = DefaultApplication.getAppContext();
                String token = AuthManager.getInstance().getToken(context);
                if(AuthManager.getInstance().isLoggedIn(context))
                    builder.addHeader("Authorization", "Token " + token);*/

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        };

        OkHttpClient okHttpClient =
                new OkHttpClient.Builder()
//                        .addNetworkInterceptor(new StethoInterceptor())
                        .addInterceptor(interceptor)
                        .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        return DataRepository.getInstance(
                RemoteDataSource.getInstance(mainUiThread, threadExecutor, apiService),
                LocalDataSource.getInstance(mainUiThread, threadExecutor),
                NetworkHelper.getInstance());
    }
}
