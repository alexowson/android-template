package com.vale.template;

import android.app.Application;
import android.content.Context;

/**
 * Created by owson on 5/29/18.
 */

public class DefaultApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return mContext;
    }
}
