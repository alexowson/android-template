package com.vale.template.ui.base;

import android.support.v4.app.Fragment;
import android.widget.Toast;


/**
 * Created by owson on 11/19/17.
 */

public abstract class BaseFragment extends Fragment implements IBaseView {
    //@BindView(R.id.progressBar)
    //protected ProgressBar progressBar;

    //@BindView(R.id.preLoaderView)
    //protected View preLoaderView;

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setProgressBar(boolean show) {
//        ProgressBar progressBar = (ProgressBar)getView().findViewById(R.id.progressBar);
//        PreLoaderView preLoaderView = (PreLoaderView)getView().findViewById(R.id.preLoaderView);

        /*if (show) {
            if(progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
            if(preLoaderView != null) {
                preLoaderView.show();
            }
        } else {
            if(progressBar != null)
                progressBar.setVisibility(View.GONE);
            if(preLoaderView != null) {
                preLoaderView.hide();
            }
        }*/
    }
}
