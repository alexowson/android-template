package com.vale.template.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.vale.template.data.DataRepository;
import com.vale.template.data.DataSource;
import com.vale.template.ui.base.BasePresenter;
import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;

/**
 * Created by owson on 5/29/18.
 */

public class MainPresenter
        extends BasePresenter<MainContract.View>
        implements MainContract.Presenter {

    private static final String TAG = MainPresenter.class.getName();

    private DataRepository dataRepository;
    private ThreadExecutor threadExecutor;
    private MainUiThread mainUiThread;

    public MainPresenter(@NonNull MainContract.View view,
                         @NonNull DataRepository dataRepository,
                         @NonNull ThreadExecutor threadExecutor,
                         @NonNull MainUiThread mainUiThread){
        this.view = view;
        this.dataRepository = dataRepository;
        this.threadExecutor = threadExecutor;
        this.mainUiThread = mainUiThread;
    }

    @Override
    public void loginUser(Context context, String email, String password) {
        Log.e(TAG, "loginUser: email="+email );
        Log.e(TAG, "loginUser: password="+password );

        /*//Hace algo
        String token = "asas232asas";
        view.showLoginSuccessFul(token);*/

        dataRepository.loginUser(context, email, password, new DataSource.GetRegisterUserCallback() {
            @Override
            public void onSuccess(String token) {
                if (view != null) {
                    view.setProgressBar(false);
                    view.showLoginSuccessFul(token);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e("SIGN_UP_PRESENTER", "onFailure: " + throwable.getMessage());
                if (view != null) {
                    view.setProgressBar(false);
                    view.showLoginError(throwable.getMessage());
                }
            }

            @Override
            public void onNetworkFailure() {
                Log.e("SIGN_UP_PRESENTER", "onNetworkFailure: ");
                view.setProgressBar(false);
            }
        });
    }
}
