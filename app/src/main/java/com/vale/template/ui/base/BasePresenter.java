package com.vale.template.ui.base;

/**
 * Created by owson on 11/19/17.
 */

public class BasePresenter<ViewT> implements IBasePresenter<ViewT> {
    protected ViewT view;


    @Override
    public void onViewActive(ViewT view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }

//    @Override
//    public void onAttach(ViewT view) {
//
//    }
}
