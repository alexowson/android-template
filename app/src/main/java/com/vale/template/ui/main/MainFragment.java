package com.vale.template.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.vale.template.R;
import com.vale.template.data.DataRepository;
import com.vale.template.di.Injection;
import com.vale.template.ui.base.BaseFragment;
import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment
        extends BaseFragment
        implements MainContract.View {

    private static final String TAG = MainFragment.class.getName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private MainPresenter presenter;

    @BindView(R.id.helloTextView)
    TextView helloTextView;

    @BindView(R.id.emailEditText)
    EditText emailEditText;

    @BindView(R.id.passwordEditText)
    EditText passwordEditText;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        ThreadExecutor threadExecutor = ThreadExecutor.getInstance();
        MainUiThread mainUiThread = MainUiThread.getInstance();
        DataRepository dataRepository = Injection.provideDataRepository(mainUiThread, threadExecutor);

        presenter = new MainPresenter(this, dataRepository, threadExecutor, mainUiThread);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);


        helloTextView.setText("");

        return view;
    }

    @OnClick(R.id.loginButton)
    public void onLoginClick () {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        presenter.loginUser(getActivity(), email, password);
    }


    @Override
    public void showLoginSuccessFul(String token) {
        Log.e(TAG, "showLoginSuccessFul: " );
        helloTextView.setText("Bienvenido");
    }

    @Override
    public void showLoginError(String message) {
        Log.e(TAG, "showLoginError: " );
        helloTextView.setText(message);
    }
}
