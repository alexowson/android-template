package com.vale.template.ui.main;

import android.content.Context;

import com.vale.template.ui.base.IBasePresenter;
import com.vale.template.ui.base.IBaseView;

/**
 * Created by owson on 5/29/18.
 */

public interface MainContract {
    interface View extends IBaseView {
        void showLoginSuccessFul(String token);
        void showLoginError(String message);
    }

    interface Presenter extends IBasePresenter<View> {
        void loginUser(Context context, String email, String password);
    }
}
