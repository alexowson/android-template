package com.vale.template.ui.base;

/**
 * Created by owson on 11/19/17.
 */

public interface IBasePresenter<ViewT> {

    void onViewActive(ViewT view);

    void onViewInactive();

//    void onAttach(ViewT view);

//    void onDetach();
}
