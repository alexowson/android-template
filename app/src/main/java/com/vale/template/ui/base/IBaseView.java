package com.vale.template.ui.base;

/**
 * Created by owson on 11/20/17.
 */

public interface IBaseView {
    void showToastMessage(String message);

    void setProgressBar(boolean show);

    //Context getContext();
}
