package com.vale.template.data;

import android.content.Context;
import android.util.Log;

import com.vale.template.utils.NetworkHelper;

/**
 * Created by owson on 2/1/18.
 */

public class DataRepository {
    private DataSource remoteDataSource;
    private DataSource localDataSource;
    private NetworkHelper networkHelper;

    private static DataRepository dataRepository;

    public DataRepository(DataSource remoteDataSource,
                          DataSource localDataSource,
                          NetworkHelper networkHelper) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
        this.networkHelper = networkHelper;
    }

    public static synchronized DataRepository getInstance(DataSource remoteDataSource,
                                                          DataSource localDataSource,
                                                          NetworkHelper networkHelper) {
        if (dataRepository == null) {
            dataRepository = new DataRepository(remoteDataSource, localDataSource, networkHelper);
        }
        return dataRepository;
    }


    public void loginUser(Context context, String email, String password, final DataSource.GetRegisterUserCallback callback) {
        if (networkHelper.isNetworkAvailable(context)) {
            Log.e("DataRepository", "Llamando a servicio remoto");
            remoteDataSource.loginUser(email, password, callback);
        } else {
            Log.e("DataRepository", "Llamando a servicio local");
//            Log.e("DataRepository", "Network is not Available: ");
//            AlertManager.showNetworkNotAvailableAlert(context);
            localDataSource.loginUser(email, password, callback);
        }
    }
}
