package com.vale.template.data.local;

/**
 * Created by owson on 5/29/18.
 */

//@Database(name = LocalDatabase.NAME, version = LocalDatabase.VERSION)
public class LocalDatabase {
    public static final String NAME = "CarouselDb";

    public static final int VERSION = 2;
}
