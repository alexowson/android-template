package com.vale.template.data.local;

/**
 * Created by owson on 5/29/18.
 */

import com.vale.template.data.DataSource;
import com.vale.template.data.models.user.User;
import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;

import java.util.List;

public class LocalDataSource extends DataSource {

    private static LocalDataSource localDataSource;

//    private DatabaseDefinition databaseDefinition;

    /*private LocalDataSource(MainUiThread mainUiThread,
                            ThreadExecutor threadExecutor,
                            DatabaseDefinition databaseDefinition) {*/

    private LocalDataSource(MainUiThread mainUiThread,
                            ThreadExecutor threadExecutor) {
        super(mainUiThread, threadExecutor);
//        this.databaseDefinition = databaseDefinition;
    }

    /*public static synchronized LocalDataSource getInstance(MainUiThread mainUiThread,
                                                           ThreadExecutor threadExecutor,
                                                           DatabaseDefinition databaseDefinition) {*/
    public static synchronized LocalDataSource getInstance(MainUiThread mainUiThread,
                                                           ThreadExecutor threadExecutor) {
        if (localDataSource == null) {
//            localDataSource = new LocalDataSource(mainUiThread, threadExecutor, databaseDefinition);
            localDataSource = new LocalDataSource(mainUiThread, threadExecutor);
        }
        return localDataSource;
    }

    @Override
    public void loginUser(String email, String password, final GetRegisterUserCallback callback) {
        threadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                /*final List<User> users = SQLite.select()
                        .from(User.class)
                        .limit(1)
//                        .offset(0)
                        .queryList();*/

                //TODO: Alguna consulta a SQLite
                final String token = "token_local";

                mainUiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(token);
                        callback.onFailure(new Throwable());
                    }
                });
            }
        });
    }

    public void saveLocal() {

    }
}
