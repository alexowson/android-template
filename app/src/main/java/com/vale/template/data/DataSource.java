package com.vale.template.data;


import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;


/**
 * Created by owson on 2/1/18.
 */

public abstract class DataSource {
    protected MainUiThread mainUiThread;
    protected ThreadExecutor threadExecutor;

    public DataSource(MainUiThread mainUiThread, ThreadExecutor threadExecutor) {
        this.mainUiThread = mainUiThread;
        this.threadExecutor = threadExecutor;
    }

    public interface GetRegisterUserCallback {
        void onSuccess(String token);

        void onFailure(Throwable throwable);

        void onNetworkFailure();
    }

    public abstract void loginUser(String email, String password, GetRegisterUserCallback callback);
}
