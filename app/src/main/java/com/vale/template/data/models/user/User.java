package com.vale.template.data.models.user;

import com.google.gson.annotations.SerializedName;
//import com.owson.sleedz.data.models.consumer.Consumer;

/**
 * Created by owson on 2/2/18.
 */


public class User {
    @SerializedName("username")
    public String username;

    @SerializedName("email")
    public String email;

    @SerializedName("first_name")
    public String first_name;

    @SerializedName("last_name")
    public String last_name;

    /*@SerializedName("consumer")
    public Consumer consumer;*/
}
