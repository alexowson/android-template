package com.vale.template.data.remote;

import com.vale.template.data.DataSource;
import com.vale.template.data.local.LocalDataSource;
import com.vale.template.data.models.user.responses.RegisterResponse;
import com.vale.template.utils.threading.MainUiThread;
import com.vale.template.utils.threading.ThreadExecutor;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by owson on 2/1/18.
 */

public final class RemoteDataSource extends DataSource {
    private static RemoteDataSource remoteDataSource;
    private ApiService apiService;

    public RemoteDataSource(MainUiThread mainUiThread,
                            ThreadExecutor threadExecutor,
                            ApiService apiService) {
        super(mainUiThread, threadExecutor);

        this.apiService = apiService;
    }

    public static synchronized RemoteDataSource getInstance(MainUiThread mainUiThread,
                                                            ThreadExecutor threadExecutor,
                                                            ApiService apiService) {
        if (remoteDataSource == null) {

            remoteDataSource = new RemoteDataSource(mainUiThread, threadExecutor, apiService);
        }
        return remoteDataSource;
    }

    @Override
    public void loginUser(String email, String password, final GetRegisterUserCallback callback) {

        Call<RegisterResponse> call = apiService.userLogin(email, password);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()) {
                    RegisterResponse registerResponse = response.body();


                    LocalDataSource localDataSource = LocalDataSource.getInstance(mainUiThread, threadExecutor);
                    localDataSource.saveLocal();

                    if(registerResponse.isSuccess())
                        callback.onSuccess(registerResponse.getToken());
                    else {
                        String message = registerResponse.getMessage();
                        if(registerResponse.getErrors() != null){

                            for (Map.Entry<String, List<String>> entry : registerResponse.getErrors().entrySet()) {
                                String key = entry.getKey();
                                List<String> error_messages = entry.getValue();
                                for(String error_message: error_messages)
                                    message += "\n" + key + ": " + error_message;
                            }
                        }
                        callback.onFailure(new Throwable(message));
                    }
                } else {
                    callback.onFailure(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}
