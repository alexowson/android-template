package com.vale.template.data.models.user.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

/**
 * Created by owson on 2/3/18.
 */

public class RegisterResponse {
    @SerializedName("success")
    @Expose
    private boolean success = false;

    @SerializedName("token")
    @Expose
    private String token = null;

    @SerializedName("msg")
    @Expose
    private String message = null;

    @SerializedName("errors")
    @Expose
    private HashMap<String, List<String>> errors = null;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HashMap<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, List<String>> errors) {
        this.errors = errors;
    }
}