package com.vale.template.data.remote;

import com.vale.template.data.models.user.responses.RegisterResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by owson on 2/1/18.
 */

public interface ApiService {

    @POST("users/login/")
    @FormUrlEncoded
    Call<RegisterResponse> userLogin(@Field("email") String email, @Field("password") String password);
}
